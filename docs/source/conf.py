# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

import sphinx_rtd_theme

# -- Project information -----------------------------------------------------

project = 'nutpy'
copyright = '2022, Juan Bermejo'
author = 'Juan Bermejo'

# The full version, including alpha/beta/rc tags
release = '0.2.2'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
              'sphinx_rtd_theme',
              'sphinx.ext.autodoc',
              'sphinx.ext.autosummary',
              # enables Sphinx to understand docstrings written in two
              # other popular formats: NumPy and Google
              'sphinx.ext.napoleon',
              # enables to use markdown files
              'myst_parser'
              ]

autosummary_generate = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_context = {
                "sidebar_external_links_caption": "Links",
                "sidebar_external_links": [
                    (
                        '<i class="fa fa-gitlab fa-fw"></i> Source code',
                        "https://gitlab.com/nutpy/nutpy",
                    ),
                    (
                        '<i class="fa fa-bug fa-fw"></i> Issue tracker',
                        "https://gitlab.com/nutpy/nutpy/-/issues",
                    ),
                    (
                        '<i class="fa fa-file-text fa-fw"></i> Citation',
                        "https://rdcu.be/cU9zP",
                    ),
                ],
            }

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']
