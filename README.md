nutpy
======

Author
======

Juan Bermejo Ballesteros (juan.bermejo@protonmail.com)


Description
===========

nutpy is a set of tools to study how space-telescopes with spin-precession scan strategies observe the sky. It has been developed by students from the Technical University from Madrid (Spain).


Installation
============

Please check the [Installation Guide](https://gitlab.com/phd-jb/nutpy/nutpy/-/wikis/Installation-guide) in the project [wiki](https://gitlab.com/phd-jb/nutpy/nutpy/-/wikis/home).



Documentation
=============

Basic documentation can be found [here](https://nutpy.readthedocs.io/) and some examples can also be found in [binder](https://mybinder.org/v2/gl/phd-jb%2Fnutpy%2Fnutapp/HEAD).


Contributing
=============

If you are interested in contributing to nutpy in someway, just contact me! You can find my email at the top and bottom of this readme.


Problems
========

If you find any problem using mubody (quite probable to be fair), please
open an issue in the [issue tracker](https://gitlab.com/phd-jb/nutpy/nutpy/-/issues).


Citation
========

If you have used mubody, please cite us using:

    Bermejo-Ballesteros, Juan, et al. "Visibility study in a chief-deputy formation for CMB polarization missions." The Journal of the Astronautical Sciences 69.3 (2022): 651-691.


License
=======

nutpy is released under the MIT license, hence allowing commercial use of the library. Please refer to the [LICENSE](https://gitlab.com/phd-jb/nutpy/nutpy/-/blob/main/LICENSE.txt) file

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/phd-jb/nutpy/nutpy/-/blob/main/LICENSE.txt)


Contact
=======

For any questions you can contact us using:

    juan.bermejo@protonmail.com
